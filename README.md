# Dvc Nextcloud Test

Testing `dvc-webdav` to connect with `nextcloud`

## Initialization

### When first initializing `dvc`

``` bash
./install_dvc_env.sh # creating environment and installing `dvc`
source .venv/bin/activate # activate `dvc` environment
```

### Connecting `dvc` with `webdav` (Nextcloud)

- Create one folder but 2 different sharing links, one readonly and another with editing permissions. 
  - The readonly one is so that users can do `dvc pull`. The config will be in `.dvc/config`
  - The editable one is for people that are allowed to do `dvc push`. The config will be in `.dvc/config.local`
- Add remotes: (see this [issue](https://github.com/iterative/dvc/issues/4988), particularly this [comment](https://github.com/iterative/dvc/issues/4988#issuecomment-735402100) for guidance):
  - Either do `dvc remote modify [--local]` or go straight to the `.dvc/config[.local]` to add the fields
  - The `url` field is `url = webdavs://ann.nl.tab.digital/public.php/webdav`
  - For both, take note of the links on the web version, the one after `s/<LINK-ID>` is supposed to be `user` field.
  - Also create passwords for both (though only the editable one is necessary)
	- The password for the readonly remote link is available inside its config and will be committed (just a small insignificant precaution against link bruiteforce) 
	- The password for the editable one is saved inside `.dvc/config.local` and will **not** be committed. Share this with necessary parties only. 
- Then remember to:

  ``` bash 
  git add .dvc/config
  git commit -m <message>
  ```

### Cloning repo and pulling data down 

- After cloning, install the environment and activate like above

  ``` bash
  ./install_dvc_env.sh # creating environment and installing `dvc`
  source .venv/bin/activate # activate `dvc` environment
  ```

- Then do `dvc pull -r readonly-storage`

### Some further considerations 

- Might also need `remote modify [--local] jobs <N>` to limit running too many processes that could cause timeout (see this [issue](https://github.com/iterative/dvc/issues/4121)), and/or increase timeout with `remote modify [--local] timeout <S>` to deal with it. 

## Normal workflow

- Pushing:

  ``` bash
  dvc add data/{monday,tuesday}
  git add data/{monday.dvc,tuesday.dvc}

  dvc push -r editable-storage
  git push
  ```

- For public pulling:

  ``` bash 
  git pull
  dvc pull -r readonly-storage`
  ```

- Update tracked files, see [this](https://dvc.org/doc/user-guide/how-to/update-tracked-data), first use `unprotect` then make changes then `add` + `push` again
- As of now, unclear whether to refer the `core` remote (i.e. `--default` one) as the readonly or the editable one. 
